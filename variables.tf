variable "vpc_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

variable "vpc_name" {
  description = "(Required) Name associated with the VPC to distinct from others."
  type        = string
}

variable "vpc_tags" {
  description = "A mapping of tags to assign to the all underlying resources."
  type        = map(string)
  default     = {}
}

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC."
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC."
  type        = string
  default     = "default"
}

variable "vpc_enable_internet_gateway" {
  description = "Whether to create the Internet Gateway in a VPC to allow public access for resources reside in."
  type        = bool
  default     = true
}

variable "vpc_enable_dns_support" {
  description = "A boolean flag to enable/disable DNS support in the VPC."
  type        = bool
  default     = true
}

variable "vpc_enable_dns_hostnames" {
  description = "A boolean flag to enable/disable DNS hostnames in the VPC."
  type        = bool
  default     = true
}

variable "vpc_enable_classiclink" {
  # Only valid in regions and accounts that support EC2 Classic. See the ClassicLink documentation for more
  # information at: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/vpc-classiclink.html
  description = "A boolean flag to enable/disable ClassicLink for the VPC."
  type        = bool
  default     = false
}
variable "vpc_enable_classiclink_dns_support" {
  # Only valid in regions and accounts that support EC2 Classic.
  description = "A boolean flag to enable/disable ClassicLink DNS Support for the VPC."
  type        = bool
  default     = false
}
