# Whether to create the resources. `false` prevents the module from creating any resources.
vpc_module_enabled = true
# (Required) Name associated with the VPC to distinct from others.
vpc_name  = "test4"
//# A mapping of tags to assign to the all underlying resources.
//vpc_tags = {}
//# The CIDR block for the VPC.
//vpc_cidr_block = "10.0.0.0/16"
//# A tenancy option for instances launched into the VPC.
//vpc_instance_tenancy = "default"
# Whether to create the Internet Gateway in a VPC to allow public access for resources reside in.
vpc_enable_internet_gateway = false
//# A boolean flag to enable/disable DNS support in the VPC.
//vpc_enable_dns_support = true
//# A boolean flag to enable/disable DNS hostnames in the VPC.
//vpc_enable_dns_hostnames = true
//# A boolean flag to enable/disable ClassicLink for the VPC.
//# Only valid in regions and accounts that support EC2 Classic. See the ClassicLink documentation for more
//# information at: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/vpc-classiclink.html
//vpc_enable_classiclink = false
//# A boolean flag to enable/disable ClassicLink DNS Support for the VPC.
//vpc_enable_classiclink_dns_support = false
