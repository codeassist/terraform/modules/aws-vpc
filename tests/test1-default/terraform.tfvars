//# Whether to create the resources. `false` prevents the module from creating any resources.
//vpc_module_enabled = false
# (Required) Name associated with the VPC to distinct from others.
vpc_name  = "test1"
//# A mapping of tags to assign to the all underlying resources.
//vpc_tags = {}
//# The CIDR block for the VPC.
//vpc_cidr_block = "10.0.0.0/16"
//# Whether to create the Internet Gateway in a VPC to allow public access for resources reside in.
//vpc_internet_gateway_enabled = true
//# A tenancy option for instances launched into the VPC.
//vpc_instance_tenancy = "default"
//# A boolean flag to enable/disable DNS support in the VPC.
//vpc_enable_dns_support = true
//# A boolean flag to enable/disable DNS hostnames in the VPC.
//vpc_enable_dns_hostnames = true
//# A boolean flag to enable/disable ClassicLink for the VPC.
//# Only valid in regions and accounts that support EC2 Classic. See the ClassicLink documentation for more
//# information at: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/vpc-classiclink.html
//vpc_enable_classiclink = false
//# A boolean flag to enable/disable ClassicLink DNS Support for the VPC.
//vpc_enable_classiclink_dns_support = false
//# ---------------------------------------
//# Variables for "aws-vpc-subnets" module
//# ---------------------------------------
//# Whether to allow sub-module to create the resources.
//vpc_subnet_module_enabled = true
//# Amount of availability zones to create public/private subnets within the VPC.
//vpc_az_amount = 0
//# To calculate CIDR block for every Subnet - override the maximum amount of subnets will possibly be deployed in every AZ.
//vpc_max_subnets_per_az_count = 0
//# How many public subnets in every AZ will be created.
//vpc_public_subnets_per_az = 0
//# How many private subnets in every AZ will be created.
//vpc_private_subnets_per_az = 0
//# Boolean switch to enable/disable NAT resources.
//vpc_nat_resource_create = false
//# Enable/disable NAT resource creation in every AZ for HA scenario.
//vpc_nat_resource_per_az_enabled = false
//# What kind of NAT resource to create to allow instances in private subnets to access the Internet.
//# The only possible values are (case insensitive):
//#   * instance (default)
//#   * gateway
//vpc_nat_resource_type = "instance"
//# The type of NAT EC2 Instance.
//vpc_nat_instance_type = "t3a.micro"
//# Whether to allocate EIP to the NAT instance.
//vpc_nat_instance_static_eip = false
