# See info about resources being used:
#   * https://www.terraform.io/docs/providers/aws/r/vpc.html
#   * https://www.terraform.io/docs/providers/aws/r/internet_gateway.html
#   * https://www.terraform.io/docs/providers/aws/r/default_route_table.html

locals {
  enabled = lower(var.vpc_module_enabled) ? true : false

  vpc_name = replace(var.vpc_name, "-vpc", "") == var.vpc_name ? format("%s-vpc", var.vpc_name) : var.vpc_name

  tags = merge(
    var.vpc_tags,
    {
      terraform = true,
    },
  )
}


# ----------------------------------------
# Setup VPC and some of related resources
# ----------------------------------------
resource "aws_vpc" "this" {
  # Provides a VPC resource.
  count = local.enabled ? 1 : 0

  # (Required) The CIDR block for the VPC.
  cidr_block = var.vpc_cidr_block
  # (Optional) A tenancy option for instances launched into the VPC
  instance_tenancy = var.vpc_instance_tenancy
  # (Optional) A boolean flag to enable/disable DNS support in the VPC.
  enable_dns_support = var.vpc_enable_dns_support
  # (Optional) A boolean flag to enable/disable DNS hostnames in the VPC.
  enable_dns_hostnames = var.vpc_enable_dns_hostnames
  # (Optional) A boolean flag to enable/disable ClassicLink for the VPC. Only valid in regions and accounts that
  # support EC2 Classic. See the ClassicLink documentation for more information at:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/vpc-classiclink.html
  enable_classiclink = var.vpc_enable_classiclink
  # A boolean flag to enable/disable ClassicLink DNS Support for the VPC. Only valid in regions and accounts that
  # support EC2 Classic.
  enable_classiclink_dns_support = var.vpc_enable_classiclink_dns_support
  # (Optional) Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify
  # the range of IP addresses, or the size of the CIDR block.
  assign_generated_ipv6_cidr_block = false

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s", local.vpc_name),
    },
  )
}

# -----------------------------
# Setup VPC's Internet Gateway
# -----------------------------
resource "aws_internet_gateway" "this" {
  # Provides a resource to create a VPC Internet Gateway.

  # Note(!!!): It's recommended to denote that the AWS Instance or Elastic IP depends on the Internet Gateway.
  count = (local.enabled && var.vpc_enable_internet_gateway) ? 1 : 0

  # (Required) The VPC ID to create in.
  vpc_id = join("", aws_vpc.this.*.id)

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-igw", local.vpc_name),
    },
  )
}

# ----------------------------------------------
# Setup VPC's "Main" (or "Default") Route Table
# ----------------------------------------------
resource "aws_default_route_table" "this" {
  # Provides a resource to manage a Default VPC Routing Table.
  # Each VPC created in AWS comes with a Default Route Table that can be managed, but not destroyed. This is an advanced
  # resource, and has special caveats to be aware of when using it. It is recommended you do not use both
  # `aws_default_route_table` to manage the default route table and use the `aws_main_route_table_association`, due to
  # possible conflict in routes.

  # The `aws_default_route_table` behaves differently from normal resources, in that Terraform does not create this
  # resource, but instead attempts to "adopt" it into management. We can do this because each VPC created has
  # a Default Route Table that cannot be destroyed, and is created with a single route.

  # When Terraform first adopts the Default Route Table, it immediately removes all defined routes. It then proceeds
  # to create any routes specified in the configuration. This step is required so that only the routes specified in
  # the configuration present in the Default Route Table.

  # For more information about Route Tables, see the AWS Documentation on Route Tables:
  #   * http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html#Route_Replacing_Main_Table

  # For more information about managing normal Route Tables in Terraform, see our documentation on `aws_route_table`:
  #   * https://www.terraform.io/docs/providers/aws/r/route_table.html

  # NOTE(!) on Route Tables and Routes:
  #   Terraform currently provides both a standalone Route resource and a Route Table resource with routes defined
  #   in-line. At this time you cannot use a Route Table with in-line routes in conjunction with any Route resources.
  #   Doing so will cause a conflict of rule settings and will overwrite routes.
  count = local.enabled ? 1 : 0

  # (Required) The ID of the Default Routing Table.
  default_route_table_id = join("", aws_vpc.this.*.default_route_table_id)

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-main-rt", local.vpc_name),
    },
  )
}

# --------------------------
# Setup VPC's "Default" ACL
# --------------------------
resource "aws_default_network_acl" "this" {
  # Provides a resource to manage the default AWS Network ACL. VPC Only.
  # Each VPC created in AWS comes with a Default Network ACL that can be managed, but not destroyed. This is an advanced
  # resource, and has special caveats to be aware of when using it.

  # The `aws_default_network_acl` behaves differently from normal resources, in that Terraform does not create this
  # resource, but instead attempts to "adopt" it into management. We can do this because each VPC created has
  # a Default Network ACL that cannot be destroyed, and is created with a known set of default rules.

  # When Terraform first adopts the Default Network ACL, it immediately removes all rules in the ACL. It then proceeds
  # to create any rules specified in the configuration. This step is required so that only the rules specified in
  # the configuration are created.

  # This resource treats its inline rules as absolute; only the rules defined inline are created, and any
  # additions/removals external to this resource will result in diffs being shown. For these reasons, this resource is
  # incompatible with the `aws_network_acl_rule` resource.

  # For more information about Network ACLs, see the AWS Documentation on Network ACLs:
  #   * http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_ACLs.html
  count = local.enabled ? 1 : 0

  # (Required) The Network ACL ID to manage. This attribute is exported from aws_vpc, or manually found via
  # the AWS Console.
  default_network_acl_id = join("", aws_vpc.this.*.default_network_acl_id)

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-default-acl", local.vpc_name),
    },
  )

  ### Managing Subnets in the Default Network ACL ###
  # Within a VPC, all Subnets must be associated with a Network ACL. In order to "delete" the association between
  # a Subnet and a non-default Network ACL, the association is destroyed by replacing it with an association between
  # the Subnet and the Default ACL instead.

  # When managing the Default Network ACL, you cannot "remove" Subnets. Instead, they must be reassigned to another
  # Network ACL, or the Subnet itself must be destroyed. Because of these requirements, removing the `subnet_ids`
  # attribute from the configuration of a `aws_default_network_acl` resource may result in a reoccurring plan, until
  # the Subnets are reassigned to another Network ACL or are destroyed.

  # Because Subnets are by default associated with the Default Network ACL, any non-explicit association will show up
  # as a plan to remove the Subnet. For example: if you have a custom `aws_network_acl` with two subnets attached, and
  # you remove the `aws_network_acl` resource, after successfully destroying this resource future plans will show a diff
  # on the managed `aws_default_network_acl`, as those two Subnets have been orphaned by the now destroyed network acl
  # and thus adopted by the Default Network ACL. In order to avoid a reoccurring plan, they(subnets) will need to be
  # reassigned, destroyed, or added to the `subnet_ids` attribute of the `aws_default_network_acl` entry.

  # As an alternative to the above, you can also specify the following lifecycle configuration in your
  # `aws_default_network_acl` resource:
  #   lifecycle {
  #     ignore_changes = ["subnet_ids"]
  #   }
}

# -------------------------------------
# Setup VPC's "Default" Security Group
# -------------------------------------
resource "aws_default_security_group" "this" {
  # Provides a resource to manage the default AWS Security Group.
  # For EC2 Classic accounts, each region comes with a Default Security Group. Additionally, each VPC created in AWS
  # comes with a Default Security Group that can be managed, but not destroyed. This is an advanced resource, and has
  # special caveats to be aware of when using it.

  # The `aws_default_security_group` behaves differently from normal resources, in that Terraform does not create this
  # resource, but instead "adopts" it into management. We can do this because these default security groups cannot be
  # destroyed, and are created with a known set of default ingress/egress rules.

  # When Terraform first adopts the Default Security Group, it immediately removes all ingress and egress rules in
  # the Security Group. It then proceeds to create any rules specified in the configuration. This step is required so
  # that only the rules specified in the configuration are created.

  # This resource treats its inline rules as absolute; only the rules defined inline are created, and any
  # additions/removals external to this resource will result in diff shown. For these reasons, this resource is
  # incompatible with the `aws_security_group_rule` resource.

  # For more information about Default Security Groups, see the AWS Documentation on Default Security Groups:
  #   * http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html#default-security-group
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) The VPC ID.
  # Note(!): that changing the `vpc_id` will not restore any default security group rules that were modified, added or
  # removed. It will be left in its current state!
  vpc_id = join("", aws_vpc.this.*.id)

  //  # (Optional) Can be specified multiple times for each ingress rule. This argument is processed in
  //  # `attribute-as-blocks` mode.
  //  ## By default allow everything from the instances in the same SG ##
  //  ingress {
  //    # (Required) The start port (or ICMP type number if protocol is "icmp")
  //    from_port = 0
  //    # (Required) The end range port (or ICMP code if protocol is "icmp").
  //    to_port   = 0
  //    # (Required) The protocol. If you select a protocol of "-1" (semantically equivalent to "all", which is not a valid
  //    # value here), you must specify a "from_port" and "to_port" equal to 0. If not icmp, tcp, udp, or "-1" use
  //    # the protocol number:
  //    #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  //    protocol  = -1
  //    # (Optional) If true, the security group itself will be added as a source to this ingress rule.
  //    self      = true
  //  }
  //
  //  # (Optional, VPC only) Can be specified multiple times for each egress rule. This argument is processed in
  //  # `attribute-as-blocks` mode.
  //  ## By default allow all outgoing connections to everywhere ##
  //  egress {
  //    # (Required) The start port (or ICMP type number if protocol is "icmp")
  //    from_port   = 0
  //    # (Required) The end range port (or ICMP code if protocol is "icmp").
  //    to_port     = 0
  //    # (Required) The protocol. If you select a protocol of "-1" (semantically equivalent to "all", which is not a valid
  //    # value here), you must specify a "from_port" and "to_port" equal to 0. If not icmp, tcp, udp, or "-1" use
  //    # the protocol number:
  //    #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  //    protocol    = "-1"
  //    # (Optional) List of CIDR blocks.
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-default-sg", local.vpc_name),
    },
  )

  ### Removing `aws_default_security_group` from your configuration ###
  # Each AWS VPC (or region, if using EC2 Classic) comes with a Default Security Group that cannot be deleted.
  # The `aws_default_security_group` allows you to manage this Security Group, but Terraform cannot destroy it. Removing
  # this resource from your configuration will remove it from your statefile and management, but will not destroy
  # the Security Group. All ingress or egress rules will be left as they are at the time of removal. You can resume
  # managing them via the AWS Console.
}
