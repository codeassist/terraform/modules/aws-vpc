output "vpc_id" {
  description = "The ID of the VPC."
  value       = join("", aws_vpc.this.*.id)
}

output "cidr_block" {
  description = "The CIDR block of the VPC."
  value       = join("", aws_vpc.this.*.cidr_block)
}

output "main_route_table_id" {
  description = "The ID of the main route table associated with this VPC."
  value       = join("", aws_vpc.this.*.main_route_table_id)
}

output "default_network_acl_id" {
  description = "The ID of the network ACL created by default on VPC creation."
  value       = join("", aws_vpc.this.*.default_network_acl_id)
}

output "default_security_group_id" {
  description = "The ID of the security group created by default on VPC creation."
  value       = join("", aws_vpc.this.*.default_security_group_id)
}

output "default_route_table_id" {
  description = "The ID of the route table created by default on VPC creation."
  value       = join("", aws_vpc.this.*.default_route_table_id)
}

output "igw_id" {
  description = "The ID of the Internet Gateway."
  value       = join(",", aws_internet_gateway.this.*.id)
}
