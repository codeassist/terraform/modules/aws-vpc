aws-vpc
=======

The module to provision a VPC with Internet Gateway, multi-AZ subnets and NAT gateways.


## Usage

```hcl
module "vpc" {
  source            = "git::https://gitlab.com/codeassist/terraform/modules/aws-vpc.git?ref=<tag_version>"
  vpc_name                  = "my"
  vpc_cidr_block            = "10.1.0.0/16"
  // actually only `vpc_name` would be enough. See all possible variables and their explanations in `variables.tf`
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| vpc_cidr_block | CIDR for the VPC. | string | `10.0.0.0/8` | yes |
<!-- | enable_classiclink | A boolean flag to enable/disable ClassicLink for the VPC | string | `false` | no |
| enable_classiclink_dns_support | A boolean flag to enable/disable ClassicLink DNS Support for the VPC | string | `false` | no |
| enable_dns_hostnames | A boolean flag to enable/disable DNS hostnames in the VPC | string | `true` | no |
| enable_dns_support | A boolean flag to enable/disable DNS support in the VPC | string | `true` | no |
| instance_tenancy | A tenancy option for instances launched into the VPC | string | `default` | no |
| name | Name  (e.g. `app` or `cluster`) | string | - | yes |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no | -->

## Outputs

| Name | Description |
|------|-------------|
<!-- | igw_id | The ID of the Internet Gateway |
| ipv6_cidr_block | The IPv6 CIDR block |
| vpc_cidr_block | The CIDR block of the VPC |
| vpc_default_network_acl_id | The ID of the network ACL created by default on VPC creation |
| vpc_default_route_table_id | The ID of the route table created by default on VPC creation |
| vpc_default_security_group_id | The ID of the security group created by default on VPC creation |
| vpc_id | The ID of the VPC |
| vpc_ipv6_association_id | The association ID for the IPv6 CIDR block |
| vpc_main_route_table_id | The ID of the main route table associated with this VPC. | -->
